import React from 'react';
import { DefinitionsArray } from '../../App';

import './definitions.css';

type DefinitionsProps = {
    word: string;
    language: string;
    meanings?: DefinitionsArray | undefined;
}

const Definitions = ( { word, language, meanings }: DefinitionsProps): JSX.Element => {
    if(meanings === undefined ) return (<div></div>);
    const tmpPhonetics = meanings[0]['phonetics'] || null;
    return (
        <div className='meanings'>
            {tmpPhonetics && word && language === 'en' && (
                <div className="phoneticAudio">
                    <audio 
                        src={tmpPhonetics[0]['audio']}
                        controls
                    >
                        Your browser doesn't support the audio element.
                    </audio>
                </div>
                )
            }
            
            {word === "" || meanings === undefined ? (
                <span>Plese Type a Word to Search</span>
                ) : 
                (
                    meanings.map((mean) =>
                        mean.meanings.map((item) => 
                            item.definitions.map((def) => (
                            <div className='singleMean' >
                                <span>{mean.word.toUpperCase()} ({item.partOfSpeech}) </span>
                                <p>{def.definition}</p>
                                <hr style={{ backgroundColor: "black", width: "100%" }} />
                                {
                                    def.example && (
                                        <span>
                                            <b>Example: </b>{def.example}
                                        </span>
                                    )
                                }
                                {
                                    def.synonyms && (
                                        <span style={{ display: "block"}}>
                                            <b>Synonyms: </b>{
                                            def.synonyms.map((synonym) => (
                                                <span>{synonym}, </span>
                                            )
                                        )}
                                        </span>
                                    )
                                }
                                
                            </div>
                        )
                    )
                )
            )
        )}
    </div>
    )
}

export default Definitions;