import React from 'react';
import { createTheme, TextField, ThemeProvider, MenuItem } from '@material-ui/core';

import categories from '../../data/category';
import './Header.css';


type HeaderProps = {
    category: string;
    setCategory: React.Dispatch<React.SetStateAction<string>>;
    word: string;
    setWord: React.Dispatch<React.SetStateAction<string>>;
    darkMode: boolean;
}

const Header = ({ category, setCategory, word, setWord, darkMode }: HeaderProps) => {
    const darkTheme = createTheme({
        palette: {
          primary: {
              main: '#fff'
          },
          type: 'dark',
        },
      });
    
      const lightTheme = createTheme({
        palette: {
          primary: {
              main: '#000'
          },
          type: 'light',
        },
      });
    
    const handleChange = (a: string): void => {
        setCategory(a);
        setWord("");
    }

    return (
        <div className="header">
            <span className="title">{word ? word: "Word Hunt"}</span>
            <div className="inputs">
                <ThemeProvider theme={darkMode ? darkTheme : lightTheme}>
                    <TextField 
                        className="search"
                        label="Search Dictionary" 
                        value={word}
                        onChange={(e) => setWord(e.target.value)}
                    />
                    <TextField
                        className="select"
                        select
                        label="Language"
                        value={category}
                        onChange={(e) => handleChange(e.target.value)}
                        >
                          {
                            categories.map((category): JSX.Element => (
                                <MenuItem key={category.label} value={category.label}>
                                    {category.value}
                                </MenuItem>
                              )
                            )
                          }                          
                    </TextField>
                </ThemeProvider>
            </div>
        </div>
    )
}

export default Header;