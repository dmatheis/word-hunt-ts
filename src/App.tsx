import { Container, Switch, styled, alpha } from '@material-ui/core';
import { grey } from '@material-ui/core/colors';
import axios from 'axios';
import React, { useEffect, useState } from 'react';

import Header from './components/Header/Header';
import Definitions from './components/definitions/Definitions';

import './App.css';

export type DefinitionsArray = {
  meanings: {
    definitions: {
      antonyms?: string[];
      definition: string;
      example?: string;
      synonyms?: string[];
    }[];
    partOfSpeech: string;
  }[];
  origin?: string;
  phonetic?: string;
  phonetics?: {
    text?: string;
    audio?: string;
  }[];
  word: string;
}[];

const initialMeanings: DefinitionsArray = [
  {
    meanings: [ 
      {
        definitions: [
          {
            definition: ""
          }
        ],
        partOfSpeech: ""
      }
    ],
    word: ""
  },
];

function App() {
  const [word, setWord] = useState<string>("");
  const [language, setLanguage] = useState<string>("en");
  const [meanings, setMeanings] = useState<DefinitionsArray>(initialMeanings);
  const [darkMode, setDarkMode] = useState<boolean>(true);

  const DarkMode = styled(Switch)(({ theme }) => ({
    '& .MuiSwitch-switchBase.Mui-checked': {
      color: grey[600],
      '&:hover': {
        backgroundColor: alpha(grey[600], theme.palette.action.hoverOpacity),
      },
    },
    '& .MuiSwitch-switchBase.Mui-checked + .MuiSwitch-track': {
      backgroundColor: grey[600],
    },
  }));

  const dictionaryApi = async() => {
    try {
      if(word === "") return;
      const data = await axios.get(`https://api.dictionaryapi.dev/api/v2/entries/${language}/${word}`);
      setMeanings(data.data);
    } catch (error) {
      console.log(error);
    }
  
  };
  
  console.log('Meanings', meanings);

  useEffect(() => {
    dictionaryApi();
  // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [word,language]);

  return (
    <div className={darkMode ? "App-Dark" : "App-Light"}>
      <Container 
        className="appContainer"
        maxWidth='md'
      > 
        <div className="themeSwitch">
          <span>{darkMode ? "Dark" : "Light"} Mode</span>
          <DarkMode checked={darkMode} onChange={(e) => setDarkMode(!darkMode)} />
        </div>
        <Header category={language} 
          setCategory={setLanguage} 
          word={word} 
          setWord={setWord}
          darkMode={darkMode} />
        
        { meanings && (
          <Definitions word={word} language={language} meanings={meanings}/>
        )}
      </Container>
    </div>
  );
}

export default App;
